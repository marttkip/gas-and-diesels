<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Company_invoices extends MX_Controller
{ 
	
	function __construct()
	{
		parent:: __construct();
		$this->load->model('admin/users_model');
		$this->load->model('company_invoices_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/admin_model');
		$this->load->model('administration/personnel_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('accounts/accounts_model');
		$this->load->model('accounting/creditors_model');
		$this->load->model('inventory/stores_model');
		$this->load->model('reception/database');
		$this->load->model('reception/reception_model');
	}


	/*
	*
	*	Default action is to show all the orders
	*
	*/
	public function index() 
	{
		// get my approval roles

		$where = 'invoices.invoice_status_id = order_status.order_status_id AND invoices.company_id > 0';
		$table = 'invoices, order_status';
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = base_url().'accounting/company-invoices';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = 4;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active">';
		$config['cur_tag_close'] = '</li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["links"] = $this->pagination->create_links();
		$query = $this->company_invoices_model->get_all_orders($table, $where, $config["per_page"], $page);
		
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$v_data['order_status_query'] = $this->company_invoices_model->get_order_status();
		// $v_data['level_status'] = $this->orders_model->order_level_status();
		$v_data['suppliers_query'] = $this->company_invoices_model->all_companies();
		$v_data['title'] = "All Orders";
		$data['content'] = $this->load->view('company_invoices/all_invoices', $v_data, true);
		
		$data['title'] = 'All orders';
		
		$this->load->view('admin/templates/general_page', $data);
	}


	public function add_client_invoice() 
	{
		//form validation rules
		$this->form_validation->set_rules('order_instructions', 'Order Instructions', 'required|xss_clean');
		$this->form_validation->set_rules('company_id', 'Company', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			$order_id = $this->company_invoices_model->add_invoice();
			//update order
			if($order_id > 0)
			{
				$this->session->set_userdata('success_message', 'You have successfully added a client invoice');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update order. Please try again');
			}
		}

		redirect('procurement/general-orders');
		
		
	}


	public function add_invoice_item($invoice_id,$invoice_number)
    {

		$this->form_validation->set_rules('product_id', 'Product', 'required|xss_clean');
		$this->form_validation->set_rules('quantity', 'Quantity', 'required|numeric|xss_clean');
		// $this->form_validation->set_rules('in_stock', 'In Stock', 'numeric|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			if($this->company_invoices_model->add_invoice_item($invoice_id))
			{
				$this->session->set_userdata('success_message', 'Order created successfully');
			}	
			else
			{
				$this->session->set_userdata('error_message', 'Something went wrong, please try again');
			}
		}
		else
		{

		}

		$order_details = $this->company_invoices_model->get_invoice_details($invoice_id);
		$company_name = '';
		$is_vatable = 0;
		if($order_details->num_rows() > 0)
		{
			foreach ($order_details->result() as $key => $value) {
				# code...
				$company_id = $value->company_id;
				$company_name = $value->company_name;
				$is_vatable = $value->is_vatable;
			}
		}

		$v_data['title'] = 'Add Invoice Item to '.$invoice_number;
		$v_data['order_status_query'] = $this->company_invoices_model->get_order_status();
		
		$v_data['charges_query'] = $this->company_invoices_model->all_invoice_charges();
		$v_data['invoice_number'] = $invoice_number;
		$v_data['invoice_id'] = $invoice_id;
		$v_data['company_name'] = $company_name;
		$v_data['is_vatable'] = $is_vatable;
		$v_data['invoice_item_query'] = $this->company_invoices_model->get_invoice_items($invoice_id);
		// $v_data['order_suppliers'] = $this->orders_model->get_order_suppliers($order_id);
		// $v_data['suppliers_query'] = $this->suppliers_model->all_suppliers();
		$v_data['suppliers_query'] = $this->company_invoices_model->all_companies();
		$data['content'] = $this->load->view('company_invoices/invoice_items', $v_data, true);

		$this->load->view('admin/templates/general_page', $data);
    }

    public function update_invoice_item($invoice_id,$invoice_number,$invoice_item_id)
    {
    	$this->form_validation->set_rules('quantity', 'Quantity', 'numeric|required|xss_clean');
    	$this->form_validation->set_rules('invoice_item_price', 'Price', 'numeric|required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
	    	if($this->company_invoices_model->update_invoice_item($invoice_id,$invoice_item_id))
			{
				$this->session->set_userdata('success_message', 'invoice Item updated successfully');
			}	
			else
			{
				$this->session->set_userdata('error_message', 'invoice Item was not updated');
			}
		}
		else
		{
			$this->session->set_userdata('success_message', 'Sorry, Please enter a number in the field');
		}
		redirect('accounting/add-invoice-item/'.$invoice_id.'/'.$invoice_number.'');

    }
    public function update_company_invoice($invoice_id,$is_vatable)
    {
    	$update_array['is_vatable'] = $is_vatable;

    	$this->db->where('invoice_id',$invoice_id);
    	$this->db->update('invoices',$update_array);
    	$result['message'] = 'success';
    	echo json_encode($result);
    }

    public function print_client_invoice($invoice_id)
	{

		// var_dump($order_id); die();
		$v_data['contacts'] = $this->site_model->get_contacts();
		$order_details = $this->company_invoices_model->get_invoice_details($invoice_id);
		$company_name = '';
		$is_vatable = 0;
		$invoice_description = '';
		$supplier_invoice_date = '';
		if($order_details->num_rows() > 0)
		{
			foreach ($order_details->result() as $key => $value) {
				# code...
				$company_id = $value->company_id;
				$company_name = $value->company_name;
				$is_vatable = $value->is_vatable;
				$invoice_number = $value->invoice_number;
				$invoice_description = $value->invoice_instructions;
				$supplier_invoice_date = $value->supplier_invoice_date;
			}
		}

		$v_data['title'] = 'Add Order Item to '.$invoice_number;
		$v_data['order_status_query'] = $this->company_invoices_model->get_order_status();
		$v_data['charges_query'] = $this->company_invoices_model->all_invoice_charges();
		$v_data['invoice_number'] = $invoice_number;
		$v_data['invoice_id'] = $invoice_id;
		$v_data['company_name'] = $company_name;
		$v_data['is_vatable'] = $is_vatable;
		$v_data['invoice_description'] = $invoice_description;
		$v_data['invoice_date'] = $supplier_invoice_date;
		$v_data['invoice_item_query'] = $this->company_invoices_model->get_invoice_items($invoice_id);
		$v_data['suppliers_query'] = $this->company_invoices_model->all_companies();


		$where = 'service_charge.service_charge_id = invoice_item.service_charge_id AND invoice_item.invoice_id = '.$invoice_id;
		$table = 'invoice_item, service_charge';

		$v_data['query'] = $this->company_invoices_model->get_invoice_detail_summary($where,$table);

		$this->load->view('company_invoices/print_invoice_items', $v_data);

	}

	public function send_invoice_for_approval($invoice_id,$invoice_status= NULL)
    {
    	if($invoice_status == NULL)
    	{
    		$invoice_status = 1;
    	}
    	else
    	{
    		$invoice_status = $invoice_status;
    	}
    	
		$this->company_invoices_model->update_invoice_status($invoice_id,$invoice_status);


		redirect('accounting/company-invoices');
    }
}