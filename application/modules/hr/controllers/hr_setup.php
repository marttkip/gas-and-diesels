<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Hr_setup extends MX_Controller 
{
	var $csv_path;
	function __construct()
	{
		parent:: __construct();
		
		$this->load->model('auth/auth_model');
		$this->load->model('site/site_model');
		$this->load->model('admin/users_model');
		$this->load->model('admin/file_model');
		$this->load->model('admin/sections_model');
		$this->load->model('admin/admin_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('hr/leave_model');
		$this->load->model('hr/hr_model');
		$this->load->model('hr/hr_setup_model');
		$this->load->model('hr/schedules_model');
		$this->load->model('admin/branches_model');
		$this->load->model('tutorial_model');
		$this->load->model('payroll/payroll_model');
		$this->csv_path = realpath(APPPATH . '../assets/csv');
		
		// if(!$this->auth_model->check_login())
		// {
		// 	redirect('login');
		// }
	}
    
	/*
	*
	*	Dashboard
	*
	*/
	public function dashboard() 
	{
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];
		
		$data['content'] = $this->load->view('dashboard', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
    
	/*
	*
	*	Edit hr configuration
	*
	*/
	public function configuration()
	{
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];
		$v_data['job_titles_query'] = $this->personnel_model->get_job_titles();
		
		$data['content'] = $this->load->view('configuration', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}


	                     ////////////// JOB TITLE//////////////////////////



	public function add_job_title()
    {

    	$this->form_validation->set_rules('job_title_name', 'Job title', 'required|xss_clean');
		
		//if form conatins valid data
		if ($this->form_validation->run())
		{
			if($this->hr_setup_model->add_job_title())
			{
				$this->session->set_userdata("success_message", "Job title added successfully");
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add job title. Please try again");
			}
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
		}
		
		redirect('configuration');
    }

	public function edit_job_title($job_title_id)
    {
    	$this->form_validation->set_rules('job_title_name', 'Job title', 'required|xss_clean');
		
		//if form conatins valid data
		if ($this->form_validation->run())
		{
			if($this->hr_model->edit_job_title($job_title_id))
			{
				$this->session->set_userdata("success_message", "Job title editted successfully");
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not edit job title. Please try again");
			}
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
		}
		
		redirect('configuration');
    }

	public function delete_job_title($job_title_id)
    {
		if($this->hr_model->delete_job_title($job_title_id))
		{
			$this->session->set_userdata("success_message", "Job title deleted successfully");
		}
		
		else
		{
			$this->session->set_userdata("error_message","Could not delete job title. Please try again");
		}
		
		redirect('configuration');
    }




                              ////////////// PERSONNEL TYPE//////////////////////////



    public function add_personnel_type()
    {

    	$this->form_validation->set_rules('personnel_type_name', 'Personnel Type Name', 'required|xss_clean');
		
		//if form conatins valid data
		if ($this->form_validation->run())
		{
			if($this->hr_setup_model->add_personnel_type())
			{
				$this->session->set_userdata("success_message", "Personnel Type added successfully");
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add Personnel Type. Please try again");
			}
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
		}
		
		redirect('configuration');
    }

	public function edit_personnel_type($personnel_type_id)
    {
    	$this->form_validation->set_rules('personnel_type_name', 'Personnel Name', 'required|xss_clean');
		
		//if form conatins valid data
		if ($this->form_validation->run())
		{
			if($this->hr_setup_model->edit_personnel_type($personnel_type_id))
			{
				$this->session->set_userdata("success_message", "Personnel Name editted successfully");
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not edit Personnel Name. Please try again");
			}
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
		}
		
		redirect('configuration');
    }

	public function delete_personnel_type($personnel_type_id)
    {
		if($this->hr_setup_model->delete_personnel_type($personnel_type_id))
		{
			$this->session->set_userdata("success_message", "Personnel Name deleted successfully");
		}
		
		else
		{
			$this->session->set_userdata("error_message","Could not delete Personnel Name. Please try again");
		}
		
		redirect('configuration');
    }


     ////////////// PERSONNEL TYPE//////////////////////////



    public function add_personnel_title()
    {

    	$this->form_validation->set_rules('personnel_title_name', 'Personnel Title Name', 'required|xss_clean');
		
		//if form conatins valid data
		if ($this->form_validation->run())
		{
			if($this->hr_setup_model->add_personnel_title())
			{
				$this->session->set_userdata("success_message", "Personnel Title added successfully");
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add Personnel Title. Please try again");
			}
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
		}
		
		redirect('configuration');
    }

	public function edit_personnel_title($personnel_title_id)
    {
    	$this->form_validation->set_rules('personnel_title_name', 'Personnel Title Name', 'required|xss_clean');
		
		//if form conatins valid data
		if ($this->form_validation->run())
		{
			if($this->hr_setup_model->edit_personnel_title($personnel_title_id))
			{
				$this->session->set_userdata("success_message", "Personnel Title Name editted successfully");
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not edit Personnel Title Name. Please try again");
			}
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
		}
		
		redirect('configuration');
    }

	public function delete_personnel_title($personnel_title_id)
    {
		if($this->hr_setup_model->delete_personnel_title($personnel_title_id))
		{
			$this->session->set_userdata("success_message", "Personnel Title Name deleted successfully");
		}
		
		else
		{
			$this->session->set_userdata("error_message","Could not delete Personnel Title Name. Please try again");
		}
		
		redirect('configuration');
    }




 ////////////// DOCUMENT TYPE//////////////////////////



    public function add_document_type()
    {

    	$this->form_validation->set_rules('document_type_name', 'Document Type Name', 'required|xss_clean');
		
		//if form conatins valid data
		if ($this->form_validation->run())
		{
			if($this->hr_setup_model->add_document_type())
			{
				$this->session->set_userdata("success_message", "Document Type added successfully");
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add Document Type. Please try again");
			}
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
		}
		
		redirect('configuration');
    }

	public function edit_document_type($document_type_id)
    {
    	$this->form_validation->set_rules('document_type_name', 'Document Type Name', 'required|xss_clean');
		
		//if form conatins valid data
		if ($this->form_validation->run())
		{
			if($this->hr_setup_model->edit_document_type($document_type_id))
			{
				$this->session->set_userdata("success_message", "Document Type Name editted successfully");
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not edit Document Type Name. Please try again");
			}
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
		}
		
		redirect('configuration');
    }

	public function delete_document_type($document_type_id)
    {
		if($this->hr_setup_model->delete_document_type($document_type_id))
		{
			$this->session->set_userdata("success_message", "Document Type Name deleted successfully");
		}
		
		else
		{
			$this->session->set_userdata("error_message","Could not delete Document Type Name. Please try again");
		}
		
		redirect('configuration');
    }

    ////////////// CIVIL STATUS//////////////////////////
    public function add_civil_status()
    {

    	$this->form_validation->set_rules('civil_status_name', 'Civil Status Name', 'required|xss_clean');
		
		//if form conatins valid data
		if ($this->form_validation->run())
		{
			if($this->hr_setup_model->add_civil_status())
			{
				$this->session->set_userdata("success_message", "Civil Status added successfully");
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add Civil Status. Please try again");
			}
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
		}
		
		redirect('configuration');
    }

	public function edit_civil_status($civil_status_id)
    {
    	$this->form_validation->set_rules('civil_status_name', 'Civil Status Name', 'required|xss_clean');
		
		//if form conatins valid data
		if ($this->form_validation->run())
		{
			if($this->hr_setup_model->edit_civil_status($civil_status_id))
			{
				$this->session->set_userdata("success_message", "Civil Status Name editted successfully");
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not edit Civil Status Name. Please try again");
			}
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
		}
		
		redirect('configuration');
    }

	public function delete_civil_status($civil_status_id)
    {
		if($this->hr_setup_model->delete_civil_status($civil_status_id))
		{
			$this->session->set_userdata("success_message", "Civil Status Name deleted successfully");
		}
		
		else
		{
			$this->session->set_userdata("error_message","Could not delete Civil Status Name. Please try again");
		}
		
		redirect('configuration');
    }

////////////// DEPARTMENTS//////////////////////////
    public function add_department()
    {

    	$this->form_validation->set_rules('department_name', 'Department Name', 'required|xss_clean');
		
		//if form conatins valid data
		if ($this->form_validation->run())
		{
			if($this->hr_setup_model->add_department())
			{
				$this->session->set_userdata("success_message", "Department added successfully");
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add Department. Please try again");
			}
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
		}
		
		redirect('configuration');
    }

	public function edit_department($department_id)
    {
    	$this->form_validation->set_rules('department_name', 'Department Name', 'required|xss_clean');
		
		//if form conatins valid data
		if ($this->form_validation->run())
		{
			if($this->hr_setup_model->edit_department($department_id))
			{
				$this->session->set_userdata("success_message", "Department Name editted successfully");
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not edit Department Name. Please try again");
			}
		}
		else
		{
			$this->session->set_userdata("error_message", validation_errors());
		}
		
		redirect('configuration');
    }

	public function delete_department($department_id)
    {
		if($this->hr_setup_model->delete_department($department_id))
		{
			$this->session->set_userdata("success_message", "Department Name deleted successfully");
		}
		
		else
		{
			$this->session->set_userdata("error_message","Could not delete Department Name. Please try again");
		}
		
		redirect('configuration');
    }










	
	//payroll data import
	public function import_payroll()
	{
		$v_data['title'] = $data['title'] = $this->site_model->display_page_title();
		
		$data['content'] = $this->load->view('import/import_payroll', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	//template for the payroll data
	function import_payroll_template()
	{
		//export products template in excel 
		$this->hr_model->import_payroll_template();
	}
	//import the payroll data
	function do_payroll_import()
	{
		if(isset($_FILES['import_csv']))
		{
			if(is_uploaded_file($_FILES['import_csv']['tmp_name']))
			{
				//import products from excel 
				$response = $this->hr_model->import_csv_payroll($this->csv_path);
				
				if($response == FALSE)
				{
					$v_data['import_response_error'] = 'Something went wrong. Please try again.';
				}
				
				else
				{
					if($response['check'])
					{
						$v_data['import_response'] = $response['response'];
					}
					
					else
					{
						$v_data['import_response_error'] = $response['response'];
					}
				}
			}
			
			else
			{
				$v_data['import_response_error'] = 'Please select a file to import.';
			}
		}
		
		else
		{
			$v_data['import_response_error'] = 'Please select a file to import.';
		}
		
		$v_data['title'] = $data['title'] = $this->site_model->display_page_title();
		
		$data['content'] = $this->load->view('import/import_payroll', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	
}
?>