<?php

class Hr_setup_model extends CI_Model 
{	
	   /////// JOB TITTLE ///////////////


	public function add_job_title()
	{

		$data['job_title_name'] = $this->input->post('job_title_name');
		
		if($this->db->insert('job_title', $data))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}
	
	public function edit_job_title($job_title_id)
	{
		$data['job_title_name'] = $this->input->post('job_title_name');
		
		$this->db->where('job_title_id', $job_title_id);
		if($this->db->update('job_title', $data))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}
	
	public function delete_job_title($job_title_id)
	{
		$this->db->where('job_title_id', $job_title_id);
		if($this->db->delete('job_title'))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}



	 /////// PERSONNEL TYPE ///////////////



	public function add_personnel_type()
	{
		
		$data['personnel_type_name'] = $this->input->post('personnel_type_name');
		
		if($this->db->insert('personnel_type', $data))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}
	
	public function edit_personnel_type($personnel_type_id)
	{
		$data['personnel_type_name'] = $this->input->post('personnel_type_name');
		
		$this->db->where('personnel_type_id', $personnel_type_id);
		if($this->db->update('personnel_type', $data))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}
	
	public function delete_personnel_type($personnel_type_id)
	{
		$this->db->where('personnel_type_id', $personnel_type_id);
		if($this->db->delete('personnel_type'))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}


	 /////// PERSONNEL TITLE ///////////////



	public function add_personnel_title()
	{
		
		$data['title_name'] = $this->input->post('personnel_title_name');
		
		if($this->db->insert('title', $data))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}
	
	public function edit_personnel_title($personnel_title_id)
	{
		$data['title_name'] = $this->input->post('personnel_title_name');
		
		$this->db->where('title_id', $personnel_title_id);
		if($this->db->update('title', $data))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}
	
	public function delete_personnel_title($personnel_title_id)
	{
		$this->db->where('title_id', $personnel_title_id);
		if($this->db->delete('title'))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}



	/////// DOCUMENT TYPE ///////////////



	public function add_document_type()
	{
		
		$data['document_type_name'] = $this->input->post('document_type_name');
		
		if($this->db->insert('document_type', $data))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}
	
	public function edit_document_type($document_type_id)
	{
		$data['document_type_name'] = $this->input->post('document_type_name');
		
		$this->db->where('document_type_id', $document_type_id);
		if($this->db->update('document_type', $data))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}
	
	public function delete_document_type($document_type_id)
	{
		$this->db->where('document_type_id', $document_type_id);
		if($this->db->delete('document_type'))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}



	/////// CIVIL STATUS ///////////////

	public function add_civil_status()
	{
		
		$data['civil_status_name'] = $this->input->post('civil_status_name');
		
		if($this->db->insert('civil_status', $data))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}
	
	public function edit_civil_status($civil_status_id)
	{
		$data['civil_status_name'] = $this->input->post('civil_status_name');
		
		$this->db->where('civil_status_id', $civil_status_id);
		if($this->db->update('civil_status', $data))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}
	
	public function delete_civil_status($civil_status_id)
	{
		$this->db->where('civil_status_id', $civil_status_id);
		if($this->db->delete('civil_status'))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}

	/////// DEPARTMENTS ///////////////

	public function add_department()
	{
		
		$data['department_name'] = $this->input->post('department_name');
		
		if($this->db->insert('departments', $data))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}
	
	public function edit_department($department_id)
	{
		$data['department_name'] = $this->input->post('department_name');
		
		$this->db->where('department_id', $department_id);
		if($this->db->update('departments', $data))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}
	
	public function delete_department($department_id)
	{
		$this->db->where('department_id', $department_id);
		if($this->db->delete('departments'))
		{
			return TRUE;
		}
		
		else
		{
			return FALSE;
		}
	}

}
?>