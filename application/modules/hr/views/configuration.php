		<div class="row">
        	<?php
				$success = $this->session->userdata('success_message');
				$error = $this->session->userdata('error_message');
				
				if(!empty($success))
				{
					echo '
						<div class="alert alert-success">'.$success.'</div>
					';
					
					$this->session->unset_userdata('success_message');
				}
				
				if(!empty($error))
				{
					echo '
						<div class="alert alert-danger">'.$error.'</div>
					';
					
					$this->session->unset_userdata('error_message');
				}
				
			?>
        <div class="col-md-12">
        	<!-- Job config -->
            <div class="col-md-6">
                <section class="panel">
                    <header class="panel-heading">
                        <h2 class="panel-title">Job Titles</h2>
                    </header>
                    <div class="panel-body">
                        <?php echo form_open('configuration/add-job-title');?>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <input type="text" class="form-control" name="job_title_name" placeholder="Job title">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-4">
                                <button class="btn btn-primary btn-sm" type="submit">Add job title</button>
                            </div>
                        </div>
                        
                        <?php echo form_close();?>


                        <?php
                            if($job_titles_query->num_rows() > 0)
                            {
                                $job_titles = $job_titles_query->result();
                                
                                foreach($job_titles as $res)
                                {
                                    $job_title_id = $res->job_title_id;
                                    $job_title_name = $res->job_title_name;
                                    ?>
                                    <?php echo form_open('configuration/edit-job-title/'.$job_title_id);?>
                                    <div class="row" style="margin-top:2%;">
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                                <div class="col-lg-12">
                                                    <input type="text" class="form-control" name="job_title_name" value="<?php echo $job_title_name;?>">
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-2">

                                            <button class="btn btn-success btn-xs" type="submit"><i class='fa fa-pencil'></i> Edit</button>
                                        </div>
                                        
                                        <div class="col-md-2">
                                            <a href="<?php echo site_url("configuration/delete-job-title/".$job_title_id);?>" onclick="return confirm('Do you want to delete <?php echo $job_title_name;?>?');" title="Delete <?php echo $job_title_name;?>"class='btn btn-danger btn-xs'><i class='fa fa-trash'></i> Delete</a>
                                        </div>
                                    </div>
                                    <?php echo form_close();?>
                                    <?php
                                }
                            }
                        ?>
                    </div>
                </section>
            </div>
            <!-- End job config -->

           

            <!-- Job config -->
            <div class="col-md-6">
                <section class="panel">
                    <header class="panel-heading">
                        <h2 class="panel-title">Personnel Type Titles</h2>
                    </header>
                    <div class="panel-body">
                        <?php echo form_open('configuration/add-personnel-type');?>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <input type="text" class="form-control" name="personnel_type_name" placeholder="Personnel Type">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-4">
                                <button class="btn btn-primary btn-sm" type="submit">Add Personnel Type</button>
                            </div>
                        </div>
                        
                        <?php echo form_close();?>
                        <?php
                            if($personnel_types_query->num_rows() > 0)
                            {
                                $personnel_types = $personnel_types_query->result();
                                
                                foreach($personnel_types as $res)
                                {
                                    $personnel_type_id = $res->personnel_type_id;
                                    $personnel_type_name = $res->personnel_type_name;
                                    ?>
                                    <?php echo form_open('configuration/edit-personnel-type/'.$personnel_type_id);?>
                                    <div class="row" style="margin-top:2%;">
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                                <div class="col-lg-12">
                                                    <input type="text" class="form-control" name="personnel_type_name" value="<?php echo $personnel_type_name;?>">
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-2">
                                            <button class="btn btn-success btn-xs" type="submit"><i class='fa fa-pencil'></i> Edit</button>
                                        </div>
                                        
                                        <div class="col-md-2">
                                            <a href="<?php echo site_url("configuration/delete-personnel-type/".$personnel_type_id);?>" onclick="return confirm('Do you want to delete <?php echo $personnel_type_name;?>?');" title="Delete <?php echo $personnel_type_name;?>"class='btn btn-danger btn-xs'><i class='fa fa-trash'></i> Delete</a>
                                        </div>
                                    </div>
                                    <?php echo form_close();?>
                                    <?php
                                }
                            }
                        ?>
                    </div>
                </section>
            </div>
            <!-- End job config -->
        </div>
        <div class="col-md-12">
              <!-- Job config -->
            <div class="col-md-6">
                <section class="panel">
                    <header class="panel-heading">
                        <h2 class="panel-title">Document Types</h2>
                    </header>
                    <div class="panel-body">
                        <?php echo form_open('configuration/add-document-type');?>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <input type="text" class="form-control" name="document_type_name" placeholder="Document Type">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-4">
                                <button class="btn btn-primary btn-sm" type="submit">Add Document Type</button>
                            </div>
                        </div>
                        
                        <?php echo form_close();?>


                        <?php
                            if($document_types_query->num_rows() > 0)
                            {
                                $document_types = $document_types_query->result();
                                
                                foreach($document_types as $res)
                                {
                                    $document_type_id = $res->document_type_id;
                                    $document_type_name = $res->document_type_name;
                                    ?>
                                    <?php echo form_open('configuration/edit-document-type/'.$document_type_id);?>
                                    <div class="row" style="margin-top:2%;">
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                                <div class="col-lg-12">
                                                    <input type="text" class="form-control" name="document_type_name" value="<?php echo $document_type_name;?>">
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-2">

                                            <button class="btn btn-success btn-xs" type="submit"><i class='fa fa-pencil'></i> Edit</button>
                                        </div>
                                        
                                        <div class="col-md-2">
                                            <a href="<?php echo site_url("configuration/delete-document-type/".$document_type_id);?>" onclick="return confirm('Do you want to delete <?php echo $document_type_name;?>?');" title="Delete <?php echo $document_type_name;?>"class='btn btn-danger btn-xs'><i class='fa fa-trash'></i> Delete</a>
                                        </div>
                                    </div>
                                    <?php echo form_close();?>
                                    <?php
                                }
                            }
                        ?>
                    </div>
                </section>
            </div>
            
            <!-- End job config -->


            <!-- Job config -->
            <div class="col-md-6">
                <section class="panel">
                    <header class="panel-heading">
                        <h2 class="panel-title">Personnel Titles</h2>
                    </header>
                    <div class="panel-body">
                        <?php echo form_open('configuration/add-personnel-title');?>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <input type="text" class="form-control" name="personnel_title_name" placeholder="Civil Status">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-4">
                                <button class="btn btn-primary btn-sm" type="submit">Add Personnel title</button>
                            </div>
                        </div>
                        
                        <?php echo form_close();?>


                        <?php
                            if($personnel_titles_query->num_rows() > 0)
                            {
                                $personnel_titles = $personnel_titles_query->result();
                                
                                foreach($personnel_titles as $res)
                                {
                                    $personnel_title_id = $res->title_id;
                                    $personnel_title_name = $res->title_name;
                                    ?>
                                    <?php echo form_open('configuration/edit-personnel-title/'.$personnel_title_id);?>
                                    <div class="row" style="margin-top:2%;">
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                                <div class="col-lg-12">
                                                    <input type="text" class="form-control" name="personnel_title_name" value="<?php echo $personnel_title_name;?>">
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-2">

                                            <button class="btn btn-success btn-xs" type="submit"><i class='fa fa-pencil'></i> Edit</button>
                                        </div>
                                        
                                        <div class="col-md-2">
                                            <a href="<?php echo site_url("configuration/delete-personnel-title/".$personnel_title_id);?>" onclick="return confirm('Do you want to delete <?php echo $personnel_title_name;?>?');" title="Delete <?php echo $personnel_title_name;?>"class='btn btn-danger btn-xs'><i class='fa fa-trash'></i> Delete</a>
                                        </div>
                                    </div>
                                    <?php echo form_close();?>
                                    <?php
                                }
                            }
                        ?>
                    </div>
                </section>
            </div>

            <!-- End job config -->
        </div>
        <div class="col-md-12">

            <!-- Job config -->
            <div class="col-md-6">
                <section class="panel">
                    <header class="panel-heading">
                        <h2 class="panel-title">Civil Status</h2>
                    </header>
                    <div class="panel-body">
                        <?php echo form_open('configuration/add-civil-status');?>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <input type="text" class="form-control" name="civil_status_name" placeholder="Casual Status">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-4">
                                <button class="btn btn-primary btn-sm" type="submit">Add Civil Status</button>
                            </div>
                        </div>
                        
                        <?php echo form_close();?>


                        <?php
                            if($civil_status_query->num_rows() > 0)
                            {
                                $civil_status = $civil_status_query->result();
                                
                                foreach($civil_status as $res)
                                {
                                    $civil_status_id = $res->civil_status_id;
                                    $civil_status_name = $res->civil_status_name;
                                    ?>
                                    <?php echo form_open('configuration/edit-civil-status/'.$civil_status_id);?>
                                    <div class="row" style="margin-top:2%;">
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                                <div class="col-lg-12">
                                                    <input type="text" class="form-control" name="civil_status_name" value="<?php echo $civil_status_name;?>">
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-2">

                                            <button class="btn btn-success btn-xs" type="submit"><i class='fa fa-pencil'></i> Edit</button>
                                        </div>
                                        
                                        <div class="col-md-2">
                                            <a href="<?php echo site_url("configuration/delete-civil-status/".$civil_status_id);?>" onclick="return confirm('Do you want to delete <?php echo $civil_status_name;?>?');" title="Delete <?php echo $civil_status_name;?>"class='btn btn-danger btn-xs'><i class='fa fa-trash'></i> Delete</a>
                                        </div>
                                    </div>
                                    <?php echo form_close();?>
                                    <?php
                                }
                            }
                        ?>
                    </div>
                </section>
            </div>

            <!-- End job config -->

            <!-- Job config -->
            <div class="col-md-6">
                <section class="panel">
                    <header class="panel-heading">
                        <h2 class="panel-title">Departments</h2>
                    </header>
                    <div class="panel-body">
                        <?php echo form_open('configuration/add-departments');?>
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <input type="text" class="form-control" name="department_name" placeholder="Departments">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-4">
                                <button class="btn btn-primary btn-sm" type="submit">Add Department</button>
                            </div>
                        </div>
                        
                        <?php echo form_close();?>


                        <?php
                            if($departments_query->num_rows() > 0)
                            {
                                $department = $departments_query->result();
                                
                                foreach($department as $res)
                                {
                                    $department_id = $res->department_id;
                                    $department_name = $res->department_name;
                                    ?>
                                    <?php echo form_open('configuration/edit-departments/'.$department_id);?>
                                    <div class="row" style="margin-top:2%;">
                                        <div class="col-sm-8">
                                            <div class="form-group">
                                                <div class="col-lg-12">
                                                    <input type="text" class="form-control" name="department_name" value="<?php echo $department_name;?>">
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-2">

                                            <button class="btn btn-success btn-xs" type="submit"><i class='fa fa-pencil'></i> Edit</button>
                                        </div>
                                        
                                        <div class="col-md-2">
                                            <a href="<?php echo site_url("configuration/delete-departments/".$department_id);?>" onclick="return confirm('Do you want to delete <?php echo $department_name;?>?');" title="Delete <?php echo $civil_status_name;?>"class='btn btn-danger btn-xs'><i class='fa fa-trash'></i> Delete</a>
                                        </div>
                                    </div>
                                    <?php echo form_close();?>
                                    <?php
                                }
                            }
                        ?>
                    </div>
                </section>
            </div>

            <!-- End job config -->
        </div>
</div>